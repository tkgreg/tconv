# Converter from a Java POJO class to a TypeScript interface #

This tool should help everyone who is trying to write a type-safe code using TypeScript as a front-end language and Java as a backend language. 

For example, you can generate TypeScript interfaces for your REST endpoints.

## How to build and run 

Just run `./gradlew build`, after that you can run tool with `java -jar ./build/lib/typescript-converter-all-1.0.jar -help` to get a help information.