package tconv.helpers;

import tconv.generator.TsGenerator;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Created by greg on 13/01/16.
 */
public class ClassesCollectorVisitor extends SimpleFileVisitor<Path> {

    private Path root;
    private Path out;

    public ClassesCollectorVisitor(Path root, Path out) {
        this.root = root;
        this.out = out;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
        try {
            if (attr.isRegularFile() && file.toString().endsWith(".class")) {
                URLClassLoader classLoader = URLClassLoader.newInstance(new URL[]{root.toUri().toURL()});
                TsGenerator.generate(Class.forName(extractFullClassName(file), true, classLoader), out);
            }
        } catch (MalformedURLException | ClassNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return CONTINUE;
    }

    private String extractFullClassName(Path file) {
        return file.toAbsolutePath().toString().replace(root.toAbsolutePath().toString() + "/", "").replaceAll("/", ".").replace(".class", "");
    }

}
