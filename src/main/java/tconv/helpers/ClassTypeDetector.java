package tconv.helpers;

import java.lang.reflect.Field;

/**
 * Created by greg on 13/01/16.
 */
public class ClassTypeDetector {

    public static String detectType(Field field) {
        String name = field.getType().getName();
        if (field.getType().isPrimitive()) {
            switch (name) {
                case "long":
                case "int":
                case "double":
                case "float":
                    return "number";
            }
        } else {
            switch (name) {
                case "java.lang.Long":
                case "java.lang.Integer":
                case "java.lang.Double":
                case "java.lang.Float":
                    return "number";
                case "java.lang.String":
                    return "string";
                case "java.util.Date":
                    return "Date";
                case "java.lang.Boolean":
                    return "boolean";
            }
        }

        return name;
    }

}
