package tconv;

import org.apache.commons.cli.*;
import tconv.helpers.ClassesCollectorVisitor;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws Exception {
        Options options = new Options();
        Option classesFolder = Option
                .builder("cf")
                .longOpt("classesFolder")
                .hasArg()
                .desc("Path to folder with classes")
                .build();
        Option outputFolder = Option
                .builder("out")
                .hasArg()
                .longOpt("outputFolder")
                .desc("Output folder for conversion")
                .build();
        Option help = new Option("h", "help", false, "print this message");

        options.addOption(classesFolder);
        options.addOption(outputFolder);
        options.addOption(help);

        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("typescript-converter", options);
                System.exit(0);
            }
            if (line.hasOption("cf") && line.hasOption("out")) {
                Path root = Paths.get(line.getOptionValue("cf"));
                Path out = Paths.get(line.getOptionValue("out"));
                ClassesCollectorVisitor visitor = new ClassesCollectorVisitor(root, out);
                Files.walkFileTree(root, visitor);
                System.out.println("Conversion completed");
            } else {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("-cf ../javaproject/build/classes/ -out ../tsproject/src/", options);
            }
        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
            System.exit(0);
        }

    }

}
