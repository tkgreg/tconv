package tconv.generator;

import tconv.common.Processor;
import tconv.common.TsEnumProcessor;
import tconv.common.TsInterfaceProcessor;

import java.nio.file.Path;
import java.util.stream.Stream;

/**
 * Created by greg on 13/01/16.
 */
public class TsGenerator {


    public static void generate(Class clazz, Path out) {
        final Stream<Processor> processorStream = Stream.of(
                new TsInterfaceProcessor(),
                new TsEnumProcessor()
        );
        processorStream.forEach(p -> p.generate(clazz, out));
    }
}
