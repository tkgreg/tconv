package tconv.common;

import tconv.helpers.ClassTypeDetector;

import java.util.stream.Stream;

/**
 * Created by greg on 14/01/16.
 */
public class TsInterfaceProcessor extends Processor {

    private static final String TS_INTERFACE_HEADER = "module %s {\n" +
            "\n    export interface %s {\n";
    private static final String TS_INTERFACE_BOTTOM = "    }\n}";


    @Override
    protected byte[] create(Class clazz) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format(TS_INTERFACE_HEADER, clazz.getPackage().getName(), clazz.getSimpleName()));
        Stream.of(clazz.getDeclaredFields()).forEach(fld -> {
            stringBuilder.append(String.format("        %s: %s;\n", fld.getName(), ClassTypeDetector.detectType(fld)));
        });
        stringBuilder.append(TS_INTERFACE_BOTTOM);
        return stringBuilder.toString().getBytes();
    }

    @Override
    public boolean isAppliable(Class clazz) {
        return !clazz.isEnum();
    }
}
