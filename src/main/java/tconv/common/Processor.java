package tconv.common;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by greg on 14/01/16.
 */
public abstract class Processor {

    public void generate(Class clazz, Path out) {
        if (isAppliable(clazz)) {
            Path file = out.resolve(clazz.getSimpleName() + ".ts");
            try {
                Files.write(file, create(clazz));
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    protected abstract byte[] create(Class clazz);

    public abstract boolean isAppliable(Class clazz);

}
