package tconv.common;

import java.util.stream.Stream;

/**
 * Created by greg on 14/01/16.
 */
public class TsEnumProcessor extends Processor {

    @Override
    protected byte[] create(Class clazz) {
        StringBuilder stringBuilder = new StringBuilder(String.format("module %s {\n\n", clazz.getPackage().getName()));
        stringBuilder.append(String.format("    export enum %s {\n\n", clazz.getSimpleName()));
        Stream.of(clazz.getDeclaredFields()).forEach(fld -> {
            if (!fld.getName().startsWith("$")) {
                stringBuilder.append(String.format("        %s,\n", fld.getName()));
            }
        });
        stringBuilder.append("\n");
        stringBuilder.append("    }\n");
        stringBuilder.append("}");
        return stringBuilder.toString().getBytes();
    }

    @Override
    public boolean isAppliable(Class clazz) {
        return clazz.isEnum();
    }

}
